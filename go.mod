module impractical.co/auth/grants

require (
	darlinggo.co/pan v0.1.0
	github.com/fatih/color v1.7.0 // indirect
	github.com/go-sql-driver/mysql v1.4.0 // indirect
	github.com/gobuffalo/packr v1.13.5 // indirect
	github.com/hashicorp/go-immutable-radix v1.0.0 // indirect
	github.com/hashicorp/go-memdb v0.0.0-20180223233045-1289e7fffe71
	github.com/hashicorp/go-uuid v1.0.0
	github.com/lib/pq v1.0.0
	github.com/mattn/go-colorable v0.1.2 // indirect
	github.com/mattn/go-sqlite3 v1.9.0 // indirect
	github.com/rubenv/sql-migrate v0.0.0-20180704111356-3f452fc0ebeb
	github.com/ziutek/mymysql v1.5.4 // indirect
	google.golang.org/appengine v1.1.0 // indirect
	gopkg.in/gorp.v1 v1.7.1 // indirect
	impractical.co/pqarrays v0.0.0-20170820231347-970404683d98
	yall.in v0.0.1
)
